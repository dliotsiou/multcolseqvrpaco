package projectMultColSeq
//from projectSyncPRL

import java.util.Vector
import scala.collection.mutable.HashSet

class Ant {

  //  var visitedNodes: List[Int] = Nil

  var visitedNodesSet = new java.util.HashSet[Int] //for membership checks
  var visitedNodesVector = new Vector[Int]

  var eligibleNodesSet = new HashSet[Int]
  eligibleNodesSet.++=(VRP.nodesExcDepots)

  var eligibleNodesArray: Array[Int] = eligibleNodesSet.toArray[Int]

  var distance: Int = 0 //max distance L
  var capacity: Int = 0
  var location: Int = 0
  var nextDestination: Int = 0
  var tourCost: Double = 0.0
  var load: Int = 0
  var demand: Int = 0
  var solutionDistance: Double = 0

  var routeMatrix: List[List[Int]] = Nil
  //  var routeMatrixArray = Array.ofDim[WrapDouble]() // or =routeMatrix.toArray?

  def setDistance(x: Int) = { distance = x }
  def setCapacity(x: Int) = { capacity = x }
  def setLocation(x: Int) = { location = x }

}